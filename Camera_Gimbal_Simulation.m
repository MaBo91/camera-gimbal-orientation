    %clear workspace variables
    clear    
    %Multiplot enabled
    hold on
    %Define a big coordinate system for a better view
    axis([-1.25 6.13 0 4.5 0 4.5])
    
    
    %-------------------------------------------------------------------%
    %Plot DICAS-Tunnel with patch-function([x1 x2 x3 x4], [y1 y2 y3 y4], [z1 z2 z3
    %z4])
    
    %DICAS:Ceiling
    patch([0 4.88 4.88 0], [0 0 -3.746 -3.746], [3.747 3.747 3.747 3.747], 'black')  
    %DICAS:Left Sidewall
    patch([0 4.88 4.88 0], [0 0 0 0], [0 0 3.747 3.747], 'black')
    %DICAS:Right Sidewall
    patch([0 4.88 4.88 0], [-3.747 -3.747 -3.747 -3.747], [0 0 3.747 3.747], 'black')
    %Set transparency for them (exept floor)
    alpha(0.15);
    %DICAS:Floor
    patch([0 4.88 4.88 0], [0 0 -3.746 -3.746], [0 0 0 0], 'white')
    %Plot Ellipsoid with average physical dimensions of a car body(: Lenght: 3.6m, Width:
    %1.8m, Height:1.4m)
    [x, y, z] = ellipsoid(2.44,-1.8735,0.7,1.8,0.9,0.7,12);
    surface = surf(x,y,z, 'FaceColor', [0 0 1]);
    %alpha(0.1);
    %convert surface data into face/vertices-data
    [faces, vertices, color] = surf2patch(surface);
    
    
    %-------------------------------------------------------------------%
    %Annotate driving direction
    text(-1.2,-1.8735,0,'Einfahrt', 'FontSize', 14);
    q = quiver3(-1.2,-1.8735,0,5.5,0,0);
    q.Color = 'red';
    q.LineWidth = 3;
    q.UData = 8;
    
    %-------------------------------------------------------------------%
    %Data of DICAS camera coordinates in 3D space (x,y,z)
    pos_cam1 = [0.21,0,0.93];
    pos_cam2 = [0.81,0,0.13];
    pos_cam3 = [2.19,0,0.73];
    pos_cam4 = [3.81,0,0.13];
    pos_cam5 = [4.12,0,0.93];
    pos_cam6 = [0.69,0,2.33];
    pos_cam7 = [1.71,0,1.73];
    pos_cam8 = [2.92,0,2.13];
    pos_cam9 = [0.38,0,3.13];
    pos_cam10 = [3.81,0,2.53];
    pos_cam11 = [4.42,0,3.33];
    pos_cam12 = [2.48,-0.31,3.747];
    pos_cam13 = [4.42,-0.87,3.747];
    pos_cam14 = [0.38,-2.7,3.747];
    pos_cam15 = [2.31,-3.1,3.747];
    pos_cam16 = [0.38,-3.746,3.33];
    pos_cam17 = [0.99,-3.746,2.53];
    pos_cam18 = [4.42,-3.746,3.13];
    pos_cam19 = [1.89,-3.746,2.13];
    pos_cam20 = [3.08,-3.746,1.73];
    pos_cam21 = [4.12,-3.746,2.33];
    pos_cam22 = [0.69,-3.746,0.93];
    pos_cam23 = [0.99,-3.746,0.13];
    pos_cam24 = [2.61,-3.746,0.73];
    pos_cam25 = [3.98,-3.746,0.13];
    pos_cam26 = [4.58,-3.746,0.93];
    
    pos_cam = [pos_cam1; pos_cam2; pos_cam3; pos_cam4; pos_cam5; pos_cam6; pos_cam7; pos_cam8; pos_cam9; pos_cam10;
               pos_cam11; pos_cam12; pos_cam13; pos_cam14; pos_cam15; pos_cam16; pos_cam17; pos_cam18; pos_cam19;
               pos_cam20; pos_cam21; pos_cam22; pos_cam23; pos_cam24; pos_cam25; pos_cam26];
    
%     Target Coordinates 
    centofgrav = [2.44,-1.8735,0.7]; % ellipsoid's gravity center 
    frontgrav = [3.5, -1.8735, 0.7];
    backgrav = [1.3, -1.8735, 0.7];
   
%     Camera target coordinates
    target_cam1 = backgrav;
    target_cam2 = backgrav;
    target_cam3 = centofgrav;
    target_cam4 = frontgrav;
    target_cam5 = frontgrav;
    target_cam6 = backgrav;
    target_cam7 = centofgrav;
    target_cam8 = centofgrav;
    target_cam9 = backgrav;
    target_cam10 = frontgrav;
    target_cam11 = frontgrav;
    target_cam12 = centofgrav;
    target_cam13 = centofgrav;
    target_cam14 = centofgrav;
    target_cam15 = centofgrav;
    target_cam16 = backgrav;
    target_cam17 = backgrav;
    target_cam18 = frontgrav;
    target_cam19 = centofgrav;
    target_cam20 = centofgrav;
    target_cam21 = frontgrav;
    target_cam22 = backgrav;
    target_cam23 = backgrav;
    target_cam24 = centofgrav;
    target_cam25 = frontgrav;
    target_cam26 = frontgrav;

% %v1 -> Cameras Target = Center of Gravity
%     target_cam1 = centofgrav;
%     target_cam2 = centofgrav;
%     target_cam3 = centofgrav;
%     target_cam4 = centofgrav;
%     target_cam5 = centofgrav;
%     target_cam6 = centofgrav;
%     target_cam7 = centofgrav;
%     target_cam8 = centofgrav;
%     target_cam9 = centofgrav;
%     target_cam10 = centofgrav;
%     target_cam11 = centofgrav;
%     target_cam12 = centofgrav;
%     target_cam13 = centofgrav;
%     target_cam14 = centofgrav;
%     target_cam15 = centofgrav;
%     target_cam16 = centofgrav;
%     target_cam17 = centofgrav;
%     target_cam18 = centofgrav;
%     target_cam19 = centofgrav;
%     target_cam20 = centofgrav;
%     target_cam21 = centofgrav;
%     target_cam22 = centofgrav;
%     target_cam23 = centofgrav;
%     target_cam24 = centofgrav;
%     target_cam25 = centofgrav;
%     target_cam26 = centofgrav;
  
    target_cam = [target_cam1; target_cam2; target_cam3; target_cam4; target_cam5; target_cam6; target_cam7; target_cam8; target_cam9; target_cam10;
               target_cam11; target_cam12; target_cam13; target_cam14; target_cam15; target_cam16; target_cam17; target_cam18; target_cam19;
               target_cam20; target_cam21; target_cam22; target_cam23; target_cam24; target_cam25; target_cam26];
           
    %-------------------------------------------------------------------%
    %Draw camera positions       
    for i=1:26
        scatter3(pos_cam(i,1), pos_cam(i,2), pos_cam(i,3), '.')
        str = sprintf('Cam%d', i);
        text(pos_cam(i,1), pos_cam(i,2), pos_cam(i,3), str, 'Fontsize', 9, 'Color', 'red');     
    end
    
    %-------------------------------------------------------------------%
    %Draw vectors from each camera position to its target coordinates and save them
    
    %vec_cam = zeros(26,300); %preallocate result matrix for improving
    %speed -> results wrong intersection points
    
    steps_pixelgrid = 2 ;   %select pixel grid size (0 -> no grid)
                            %(1 -> pixel grid of 8 vectors around center pixel)
                         
  
    for i=1:26
       vec = [pos_cam(i,:); target_cam(i,:)];    
       plot3(vec(:,1), vec(:,2), vec(:,3), 'r');
       
       %Save coordinate data of center pixel 
       vec_cam(i,1:3)=vec(1,:);
       vec_cam(i,4:6)=vec(2,:);
       
       %adjust pixel grid resolution
       delta_x = 0.15;
       delta_z = 0.15;
       delta_y = 0.15;
       
       %Calculate Pixel grid around center pixel and save them
       row = 7; %begin to write at row 7 (in vec_cam matrix)
       
       %grid offset for the cameras on the top has to be in x-y-direction 
       if i >= 12 && i <= 15
           delta_z = 0;
       else
           delta_y = 0;
       end
       
       for p = 1:steps_pixelgrid
        vec_cam(i,row:row+2)=[vec(2,1) vec(2,2)+delta_y*p*(-1) vec(2,3)+delta_z*p];
        row = row+3;
        vec_cam(i,row:row+2)=[vec(2,1)+delta_x*p vec(2,2)+delta_y*p*(-1) vec(2,3)+delta_z*p];
        row = row+3;
        vec_cam(i,row:row+2)=[vec(2,1)+delta_x*p vec(2,2) vec(2,3)];
        row = row+3;
        vec_cam(i,row:row+2)=[vec(2,1)+delta_x*p vec(2,2)-delta_y*p*(-1) vec(2,3)-delta_z*p];
        row = row+3;
        vec_cam(i,row:row+2)=[vec(2,1) vec(2,2)-delta_y*p*(-1) vec(2,3)-delta_z*p];
        row = row+3;
        vec_cam(i,row:row+2)=[vec(2,1)-delta_x*p vec(2,2)-delta_y*p*(-1) vec(2,3)-delta_z*p];
        row = row+3;
        vec_cam(i,row:row+2)=[vec(2,1)-delta_x*p vec(2,2) vec(2,3)];
        row = row+3;
        vec_cam(i,row:row+2)=[vec(2,1)-delta_x*p vec(2,2)-delta_y*p*(-1) vec(2,3)+delta_z*p];
        row = row+3;
       end
       
       
    end
   
   %-------------------------------------------------------------------%
   %Calculate points of intersection of each vector with ellipsoid's
   %surface and indicate them
   
   %number_of_vectors = (size(vec_cam,2)-3) /3;
   n=0; %continuous variable for indexing result matrix
   %Intersection_Coords = zeros(26*number_of_vectors, 5); %preallocate result matrix for improving speed
   row = row-1;
   
   for i=1:size(faces,1) %for every face of ellipsoid
      Face = faces(i, 1:end);
      Face_Vertex1 = vertices(Face(1),:);
      Face_Vertex2 = vertices(Face(2),:);
      Face_Vertex3 = vertices(Face(3),:);
      Face_Vertex4 = vertices(Face(4),:);
       
      for j=1:26 %go through every camera vector, at first cam position
          Cam_Vector1 = vec_cam(j,1:3);
          
          for m=4:3:row %then select target vector 
          Cam_Vector2 = vec_cam(j,m:m+2);
           
           for k=1:2 %now split face in two triangles              
               if k==1
                   P1 = Face_Vertex1;
                   P2 = Face_Vertex2;
                   P3 = Face_Vertex3;       
               elseif k==2
                   P1 = Face_Vertex1;
                   P2 = Face_Vertex3;
                   P3 = Face_Vertex4;
               end
                  %and look for intersection points in this two triangles
                  N = cross(P2-P1,P3-P1); % Normal to the plane of the triangle
                  P0 = Cam_Vector1 + dot(P1-Cam_Vector1,N)/dot(Cam_Vector2-Cam_Vector1,N)*(Cam_Vector2-Cam_Vector1); % The point of intersection  
                  
                  %store intersection point if its inside the triangle  
           if dot(cross(P2-P1,P0-P1),N)>=0 && dot(cross(P3-P2,P0-P2),N)>=0 && dot(cross(P1-P3,P0-P3),N)>=0 %&& Cam_Vector1(1,2) >= P0(1,2) >= Cam_Vector2(1,2)
                if j <= 13 %for the cameras on the left sidewall
                    if P0(1,2) <= Cam_Vector1(1,2) && P0(1,2) >= Cam_Vector2(1,2) %the intersection must be between the two y-coordinates of the camera vector   
                        Intersection_Coords(n+1,1) = i; % Face Number
                        Intersection_Coords(n+1,2) = j; % Cam Vector
                        Intersection_Coords(n+1,3:5) = P0; % Intersection Coordinates
                        n = n+1;    
                    end
                end
                
                if j >= 13 %for the cameras on the right sidewall
                    if P0(1,2) >= Cam_Vector1(1,2) && P0(1,2) <= Cam_Vector2(1,2) %the intersection must be between the two y-coordinates of the camera vector
                      Intersection_Coords(n+1,1) = i; % Face Number
                      Intersection_Coords(n+1,2) = j; % Cam Vector
                      Intersection_Coords(n+1,3:5) = P0; % Intersection Coordinates
                      n = n+1;  
                    end
                end
           end
           end       
          end   
       end
   end
   
   %calculate face-colors based on intersections for colormap
   facecolor = zeros(size(faces,1),1);
   for f=1:size(Intersection_Coords,1) 
           facecolor(Intersection_Coords(f,1),1) = facecolor(Intersection_Coords(f,1),1)+1;  
   end
   
   %plot intersection points and colorize intersected faces
   for k=1:size(Intersection_Coords,1)
       scatter3(Intersection_Coords(k,3), Intersection_Coords(k,4), Intersection_Coords(k,5), '.');
       %patch('Vertices',vertices,'Faces',faces(Intersection_Coords(k,1),:), 'FaceVertexCData', hsv(1) ,'FaceColor','flat')     
       patch('Vertices',vertices,'Faces',faces(Intersection_Coords(k,1),:), 'FaceVertexCData', facecolor(Intersection_Coords(k,1)) ,'FaceColor','flat');
       colormap('autumn');
       colorbar;
       fprintf('\nCam%i intersects Face#%i at x=%f y=%f z=%f', Intersection_Coords(k,2), Intersection_Coords(k,1), Intersection_Coords(k,3), Intersection_Coords(k,4), Intersection_Coords(k,5));
   end      

   %Figure plot settings
   cameratoolbar('SetMode', 'orbit');
   camlight;
 
   grid on
   axis equal
   
   xlabel('x');
   ylabel('y');
   zlabel('z');
   %-------------------------------------------------------------------%